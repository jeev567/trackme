package com.trackme.ups.service.impl;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.trackme.exception.TrackMeCustomException;
import com.trackme.ups.modal.TrackInfo;
import com.trackme.ups.modal.UPS;
import com.trackme.ups.repository.UpsRepository;
import com.trackme.ups.service.UpsService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TrackmeApplicationTests {
	
	@Autowired
	private UpsService upsService;
	
	@Autowired
	private UpsRepository ups;
	
	@Test
	public void testUpsService() throws TrackMeCustomException {
		
		try {
			upsService.fetchTrackingDetail("9500126506829063342766");
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	@Test
	public void testFetchAllUpsRepo() {
		
			 List<UPS> uList =  (List<UPS>) ups.findAll(); 
			 
			 for (UPS ups : uList) {
				 System.out.println("****************");
				 System.out.println("ID: "+ ups.getId());
				 List<TrackInfo> trackInfo =  ups.getTrackDetals();
				 	for (TrackInfo t : trackInfo) {
				 		System.out.println("Track Info:");
				 			System.out.println(t.getId());
				 			System.out.println(t.getTrackerId());
				 			System.out.println(t.getTrackSummary());
				 			List<String> trackDetails = t.getTrackDetail();
				 			int i = 0 ;
				 			for (String s: trackDetails) {
				 				System.out.println("Track Details: "+i+" "+ s.trim());
				 				i++;
							}
						
					}
				 
				 
			}
		
	}
}
