package com.trackme.util;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.trackme.ups.modal.TrackID;
import com.trackme.ups.modal.TrackRequest;

import junit.framework.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TrackmeApplicationTests {
	
	@SuppressWarnings("deprecation")
	@Test
	public void testURLUtilConvertToUTF_8EncodedURL() {
		String baseUrl ="http://production.shippingapis.com/ShippingAPI.dll?";
		String trackId = "9500126506829063342759";
		String expected = "http://production.shippingapis.com/ShippingAPI.dll?XML=%3C%3Fxml+version%3D%221.0%22+encoding%3D%22UTF-8%22+standalone%3D%22yes%22%3F%3E%3CTrackRequest+USERID%3D%22186INMOM0778%22%3E%3CTrackID+ID%3D%229500126506829063342759%22%2F%3E%3C%2FTrackRequest%3E&API=TrackV2"; 
		XMLUtil x = new XMLUtil();
		Map<String, String> requestParams = new HashMap<>();
		requestParams.put(Constants.UPS_HEADER_A_API,Constants.UPS_HEADER_A_API_VALUE);
		requestParams.put(Constants.UPS_HEADER_B_XML,x.convertToXml(new TrackRequest("186INMOM0778",new TrackID(trackId)), TrackRequest.class));
		Assert.assertEquals(expected,URLUtil.convertToUTF_8EncodedURL(baseUrl,requestParams));
	}

}
