package com.trackme.ups.service;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import com.trackme.exception.TrackMeCustomException;

public interface BaseService {
		Object fetchTrackingDetail(Object id) throws IOException, JAXBException, TrackMeCustomException;
}
