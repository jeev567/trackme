package com.trackme.ups.service.impl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.trackme.exception.RemoteServertException;
import com.trackme.exception.TrackMeCustomException;
import com.trackme.ups.modal.TrackID;
import com.trackme.ups.modal.TrackRequest;
import com.trackme.ups.modal.UPS;
import com.trackme.ups.repository.UpsRepository;
import com.trackme.ups.service.UpsService;
import com.trackme.util.Constants;
import com.trackme.util.URLUtil;
import com.trackme.util.XMLUtil;


@Service
public class UpsServiceImpl implements UpsService {

	@Value("${ups.username}")
	private String userId;
	
	@Value("${ups.baseurl}")
	private String baseUrl;
	
	@Autowired
	private UpsRepository upsRepository;

	@Override
	public Object fetchTrackingDetail(Object id) throws IOException, JAXBException, TrackMeCustomException {
		
		XMLUtil x = new XMLUtil();
		Map<String, String> requestParams = new HashMap<>();
		
		requestParams.put(Constants.UPS_HEADER_A_API,Constants.UPS_HEADER_A_API_VALUE);
		requestParams.put(Constants.UPS_HEADER_B_XML,x.convertToXml(new TrackRequest(userId,new TrackID((String)id)), TrackRequest.class));
		String encodedUrl = URLUtil.convertToUTF_8EncodedURL(baseUrl,requestParams);
		URL obj = new URL(encodedUrl);
		
		
		
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		int responseCode = con.getResponseCode();
		BufferedReader br;
		if (200 <= responseCode && responseCode <= 299) {
			br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			JAXBContext jaxbContext = JAXBContext.newInstance(UPS.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			String message = IOUtils.toString(br);
			InputStream is = new ByteArrayInputStream(message.getBytes());
			UPS product = (UPS)unmarshaller.unmarshal(is);
			upsRepository.save(product);
			
		} else {
			String message = IOUtils.toString(new BufferedReader(new InputStreamReader(con.getErrorStream())));
			throw new RemoteServertException(message);
		}
			

		return null;
	}
}
