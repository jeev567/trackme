package com.trackme.ups.modal;

import java.util.List;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;

@Entity
public class TrackInfo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	
	private String trackerId;

	private String trackSummary;
	
	@ManyToOne
	private UPS ups;
	
	@ElementCollection(fetch=FetchType.EAGER)
	private List<String> trackDetail;

	public String getTrackerId() {
		return trackerId;
	}

	@XmlAttribute(name = "ID")
	public void setTrackerId(String trackerId) {
		this.trackerId = trackerId;
	}

	@XmlElement(name = "TrackSummary")
	public String getTrackSummary() {
		return trackSummary;
	}

	public void setTrackSummary(String trackSummary) {
		this.trackSummary = trackSummary;
	}

	@XmlElement(name = "TrackDetail")
	public List<String> getTrackDetail() {
		return trackDetail;
	}

	public void setTrackDetail(List<String> trackDetail) {
		this.trackDetail = trackDetail;
	}

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public UPS getUps() {
		return ups;
	}

	public void setUps(UPS ups) {
		this.ups = ups;
	}

	
	

}