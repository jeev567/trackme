package com.trackme.ups.modal;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TrackID {

	private String id;

	public TrackID() {
	}

	public TrackID(String id) {
		this.id = id;
	}

	@XmlAttribute(name = "ID")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "TrackID [id=" + id + "]";
	}
	
}

