package com.trackme.ups.modal;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement(name = "TrackResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class UPS {
	
	

	@XmlElement(name = "TrackInfo")
	@OneToMany(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinColumn(name="ups_id")
	private List<TrackInfo> trackDetals = new ArrayList<TrackInfo>();
	

	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	private long id = Long.valueOf(trackDetals.get(0).getTrackerId());
	
	
	
	public List<TrackInfo> getTrackDetals() {
		return trackDetals;
	}

	public void setTrackDetals(List<TrackInfo> trackDetals) {
		this.trackDetals = trackDetals;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}