package com.trackme.ups.modal;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "TrackRequest")
public class TrackRequest {

	private TrackID trackId;
	private String userId;

	public TrackRequest() {
	}

	public TrackRequest(String userId, TrackID trackId) {
		super();
		this.userId = userId;
		this.trackId = trackId;
	}

	@XmlElement(name = "TrackID")
	public TrackID getTrackId() {
		return trackId;
	}

	public void setTrackID(TrackID trackId) {
		this.trackId = trackId;
	}

	@XmlAttribute(name = "USERID")
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
}


	
	
