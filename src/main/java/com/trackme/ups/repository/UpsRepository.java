package com.trackme.ups.repository;

import org.springframework.data.repository.CrudRepository;

import com.trackme.ups.modal.UPS;

public interface UpsRepository extends CrudRepository<UPS, Long>{

}
