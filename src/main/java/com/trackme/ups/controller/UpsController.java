package com.trackme.ups.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.trackme.exception.BlankRequestException;
import com.trackme.ups.modal.UPS;
import com.trackme.ups.repository.UpsRepository;
import com.trackme.ups.service.UpsService;

@RestController
public class UpsController {
	
	@Autowired
	private UpsService upsSrvice;
	
	@Autowired
	private UpsRepository upsRepository;

	@RequestMapping("/ups/tracknumber")
	public List<UPS> greeting(@RequestParam(value = "trackerId", defaultValue = "World") String trackerId) {
		
		try {
			if(trackerId==null || trackerId.isEmpty())throw new BlankRequestException("The request is blank..User needs to provide a tracker id.");
			
			
			upsSrvice.fetchTrackingDetail(trackerId);
			return (List<UPS>) upsRepository.findAll();
			
			
			
		}catch (Exception e) {
			List errorString  = new ArrayList<>();
			if(e instanceof BlankRequestException) {
				errorString.add("Please provide a tracker number");
				return 	errorString;
			}
			errorString.add("we apologie for the inconvinience..but we facing some technical issue..Please try after some time");
			return errorString;
		}
		//Get the track id
		//Forms a track Request
		//Make a UPS call and get the detail and save it to the database
		

	}

	
}