package com.trackme.util;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class URLUtil {
	public static String convertToUTF_8EncodedURL(String baseUrl,
			Map<String, String> requestParam) {

		StringBuilder parmaFormer = new StringBuilder();
		parmaFormer.append(baseUrl);
		if (requestParam != null && requestParam.size() > 0) {
			int i = requestParam.size() - 1;
			for (Map.Entry<String, String> params : requestParam.entrySet()) {
				parmaFormer.append(encodeValue(params.getKey())
						+ Constants.COMMONS_EQUAL
						+ (encodeValue(params.getValue())));
				if (i != 0)
					parmaFormer.append(Constants.COMMONS_AND);
				i--;
			}
		}
		return parmaFormer.toString();
	}

	public static String encodeValue(String value) {
		try {
			return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
