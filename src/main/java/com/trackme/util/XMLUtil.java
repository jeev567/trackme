package com.trackme.util;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class XMLUtil {
	public String convertToXml(Object source, Class... type) {
		String result;
		StringWriter sw = new StringWriter();
		try {
			JAXBContext obj = JAXBContext.newInstance(type);
			Marshaller carMarshaller = obj.createMarshaller();
			carMarshaller.marshal(source, sw);
			result = sw.toString();
		} catch (JAXBException e) {
			throw new RuntimeException(e);
		}
		return result;
	}
}
