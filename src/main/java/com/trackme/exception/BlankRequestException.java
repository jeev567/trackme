package com.trackme.exception;

public class BlankRequestException extends TrackMeCustomException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BlankRequestException(String s) {
		super(s);
	}
}
