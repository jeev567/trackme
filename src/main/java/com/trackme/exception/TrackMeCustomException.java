package com.trackme.exception;

public class TrackMeCustomException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TrackMeCustomException(String s) {
		super(s);
	}
}
