package com.trackme.exception;

public class RemoteServertException extends TrackMeCustomException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RemoteServertException(String s) {
		super(s);
	}
}
